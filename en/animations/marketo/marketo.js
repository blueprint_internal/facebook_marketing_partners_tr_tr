
function Marketo(resources)
{
	Marketo.resources = resources;
}
Marketo.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 800, Phaser.CANVAS, 'Marketo', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{

        this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 800;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('bckg', Marketo.resources.bckg);
    	this.game.load.image('speedo', Marketo.resources.speedo);
		this.game.load.image('videoz', Marketo.resources.videoz);
		this.game.load.image('chartz', Marketo.resources.chartz);

	},

	create: function(evt)
	{
     
    //Call the animation build function
    this.parent.buildAnimation();

	},

	cycleData: function(evt){

	},

	up: function(evt)
			{
			//	debugger
		    //console.log('button up', arguments);
	}
		,
	over: function(evt)
			{
		    //console.log('button over');
	}
		,
	out: function(evt)
			{
		    //console.log('button out');
	}
	,

	buildAnimation: function()
	{


    //Background
    this.bckg = this.game.add.sprite(0, 0, 'bckg');
   //this.bckg.anchor.set(0.5);
    
    //STYLE
    var style = {font:"bold 50px freight-sans-pro", fill: "#FFFFFF", wordWrap: false,wordWrapWidth: 500, align: "left"};

    var styleFunnel = {font:"bold 22px freight-sans-pro", fill: "#FFFFFF", wordWrap: true, wordWrapWidth: 500, align: "Center",lineSpacing: -8 };
        
        this.chartz = this.game.add.sprite(-250, 100, 'chartz');
        this.videoz = this.game.add.sprite(-250, 345, 'videoz');
		this.speedo = this.game.add.sprite(-250, 575, 'speedo');

		this.pfield_1 = this.game.add.text(this.game.world.centerX+400, 70, Marketo.resources.ico1text, styleFunnel);
		this.pfield_2 = this.game.add.text(this.game.world.centerX+400, 300, Marketo.resources.ico2text, styleFunnel);
		this.pfield_3 = this.game.add.text(this.game.world.centerX+400, 525, Marketo.resources.ico3text, styleFunnel);

	this.tweenChartz = this.game.add.tween(this.chartz).to( { x: 20 }, 1000, Phaser.Easing.Linear.Out, true, 500);
	
	this.tweenChartzText = this.game.add.tween(this.pfield_1).to( { x: this.game.world.centerX-120 }, 1500, Phaser.Easing.Linear.Out, true, 1000);

	this.tweenVideoz = this.game.add.tween(this.videoz).to( { x: 20 }, 1000, Phaser.Easing.Linear.Out, true, 1500);

	this.tweenVideozText = this.game.add.tween(this.pfield_2).to( { x: this.game.world.centerX-120 }, 1500, Phaser.Easing.Linear.Out, true, 2000);

	this.tweenSpeedo = this.game.add.tween(this.speedo).to( { x: 20 }, 1000, Phaser.Easing.Linear.Out, true, 2500);

	this.tweenSpeedoText = this.game.add.tween(this.pfield_3).to( { x: this.game.world.centerX-120 }, 1500, Phaser.Easing.Linear.Out, true, 3000);
			

	},


	actionOnClick: function(evt){

	},

	animate: function()
	{

	},

/////////////////////////////////////////////////

	update: function()
	{

	},

	render: function()
	{

	}

}